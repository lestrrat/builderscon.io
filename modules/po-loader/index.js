var po2json = require('po2json');
var utils = require('loader-utils');

module.exports = function(source) {
  this.cacheable();

  var options = utils.getOptions(this);
  if (options === null) {
    options = {};
  }
  options.stringify = false;
  options.format = 'mf';

  data = po2json.parse(source, options);
  // remove bogus key
  delete data[""]

  var res = `module.exports = ` + 
    JSON.stringify(data)
    .replace(/\u2028/g, '\\u2028')
    .replace(/\u2029/g, '\\u2029');

  return res
}

