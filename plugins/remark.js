import Vue    from 'vue'
import remark from 'remark'
import html   from 'remark-html'

// Use as $options.filters.remark(...)
Vue.filter('remark', (value) => remark()
  .use(html)
  .processSync(value).contents
)
