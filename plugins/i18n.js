import ja from '@/locales/ja/LC_MESSAGES/messages.po'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
 
Vue.use(VueI18n)
 
export default ({ app }) => {
  // use ja[key] -> value and create en[key] -> key
  var en = {}
  for (var key in ja) {
    en[key] = key
    if (ja[key] == "") {
      delete(ja[key])
    }
  }

  app.i18n = new VueI18n({
    locale: 'ja',
    fallbackLocale: 'en',
    messages: {
      'en': en,
      'ja': ja
    }
  })
}
