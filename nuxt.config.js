const axios = require('axios');
const path = require('path');

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'bulma',
    '@/assets/stylesheets/app.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/i18n.js',
    '~/plugins/remark.js'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@/hooks/build-sharaq-images'
  ],
  
  proxy: {
/*
    '/sharaq/v2019-sponsor': {
      target: 'https://sharaq-dot-builderscon-1248.appspot.com',
      secure: false,
      logLevel: 'debug',
      pathRewrite: function (path, req) {
        var path = path.replace('/sharaq/v2019-sponsor/', '').replace('?', '%3F')
        var newpath = '/?preset=v2019-sponsor&url=' + path
        return newpath
      }
    }
*/
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.resolve.modules.unshift(path.resolve(__dirname, 'modules'));
      config.module.rules.push(
        {
          'test': /\.po$/i,
          'use': [
            { 'loader': 'po-loader' }
           ]
        }
      )
    }
  },
  generate: {
    routes: function() {
      return axios.get('https://api.builderscon.io/v2/conference/list').
        then((res) => {
          return res.data.map((conference) => {
            return {
              route: '/' + conference.full_slug,
              payload: conference
            }
          })
        })
    }
  }
}
