import anime from 'animejs'

export default {
  bind (el, binding) {
    el.addEventListener('click', event => {
      const targetElement = document.querySelector(binding.value || el.hash)
      if (targetElement) {
        event.preventDefault()
        const offsetTop = targetElement.offsetTop
        scroll(offsetTop)
      }
    })
  }
}

function scroll (offsetTop) {
  anime({
    targets: 'html, body',
    scrollTop: offsetTop,
    duration: 800,
    easing: 'easeInOutSine'
  })
}
