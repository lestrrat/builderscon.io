import axios from 'axios'
import fs from 'fs'

module.exports = function() {
  this.nuxt.hook('build:before', (builder) => {
    if (!process.env.UPDATE_SPONSOR_IMAGES) {
      return
    }
    console.log("executing sharaq hooks")
    axios.
      get('https://api.builderscon.io/v2/conference/lookup_by_slug?lang=ja&slug=/builderscon/tokyo/2019').
      then((res) => {
        console.log("loaded conference builderscon tokyo 2019")
        res.data.sponsors.map((sponsor) => {
          let url = 'https://sharaq-dot-builderscon-1248.appspot.com/?preset=v2019-sponsor&url=' + sponsor.logo_url
          console.log("downloading '" + url + "'")
          axios.
            get(url, { responseType: 'arraybuffer' }).
            then((res) => {
              fs.writeFile('assets/images/sponsor/' + sponsor.id + '.jpg', Buffer.from(res.data, 'binary'), (err) => {
                if (err) throw err;
                console.log("write assets/images/index/" + sponsor.id)
              })
            })
        })
      })
  })
}


