import * as dimensions from '@/components/dimensions.js'
// import Headroom from 'vue-headroom'

export default {
  data () {
    return {
      windowMode: this.getWindowMode(),
      visibleMenu: false,
      visiblePageTop: false
    }
  },
  computed: {
    isDesktop () {
      return this.windowMode === 'Desktop'
    },
    isMobile () {
      return this.windowMode === 'Mobile'
    }
  },
  watch: {
    isDesktop (newVal) {
      if (newVal && this.visibleMenu) {
        this.visibleMenu = false
      }
    }
  },
  created () {
    if (process.browser) {
      let timer = false
      window.addEventListener('resize', () => {
        this.windowMode = this.getWindowMode()
      })
      
      let waiting = false
      window.addEventListener('scroll', () => {
        if (waiting) { return }
        waiting = true
        setTimeout(() => {
          this.visiblePageTop = dimensions.getPageYOffset() > 1000
          waiting = false
        }, 100)
      })
    }
  },
  mounted () {
//    this.$nextTick(() => {
//      const headroom = new Headroom(this.$refs.header, { offset: 500, tolerance: 5 })
//      headroom.init()
//    })
  },
  methods: {
    getWindowMode () {
      if (process.browser) {
        return (dimensions.getWindowWidth() >= 768) ? 'Desktop' : 'Mobile'
      }
      return 'Mobile'
    }
  }
}

