export const getWindowWidth = () => {
  return (window.innerWidth || document.documentElement.clientWidth || 0)
}

export const getPageYOffset = () => {
  return window.pageYOffset || document.documentElement.scrollTop
}

