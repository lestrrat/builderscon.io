import axios from 'axios'
import mixin from '@/components/mixin.js'
import cache from '@/components/cache.js'
// publicKey is obtained from the server, and is used to hash the
// cache keys
var publicKey

// load loads information about the conference from either browser
// storage, the main API server, or if debugging is enabled, the
// bundled JSON file. 
function load(slug) {
  // Make sure that the id does not have any trailing slashes
  id = slug.replace(/\/+$/, "")

  cache.load(slug, (data, err) => {
    if (!err) {
      return data
    }

    // Try API server
    axios
      .get('https://api.builderscon.io/v2/conference/lookup_by_slug?lang=ja&slug=' + encodeURIComponent(id))
      .then((response) => {
        const c = processConferenceData(response)
        cache.store(id, c, 300)
        return c
      })
      .catch(msg => throw)
  }
}


function conferenceFetchCallback(response) {
  // TODO: store this data in localStorage. 

  v.conference = response.data
  /* group sponsors into tiers */
  if (typeof v.conference === 'object' && Array.isArray(v.conference.sponsors) && v.conference.sponsors.length > 0) {
    var groups = {}
    for (var i in v.conference.sponsors) {
      var s = v.conference.sponsors[i]
      var group = groups[s.group_name]
      if (!group) {
        group = []
        groups[s.group_name] = group
      }
      group.push(s)
    }
    v.sponsors = groups
  }
}

function updateConference (v, id) {
  const isLocal = true
  // Make sure that the id does not have any trailing slashes
  id = id.replace(/\/+$/, "")

  // When run in local mode, just fetch the local
  if (process.server && isLocal) {
    fs
      .readFile('file://local/' + encodeURIComponent(id) + '.json', conferenceFetchCallback)
  } else {
    axios
      .get('https://api.builderscon.io/v2/conference/lookup_by_slug?lang=ja&slug=' + encodeURIComponent(id))
      .then(conferenceFetchCallback)
      .catch(msg => alert(msg))
  }
}

export default {
  name: 'ConferenceView',
  mixins: [ mixin ],
  data () {
    return {
      conference: {},
      sponsors: {}
    }
  },
  mounted () {
    updateConference(this, this.$route.path)
  },
  watch: {
    '$route' (to, from) {
      updateConference(this, to.path)
    }
  }
}
